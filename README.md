# concurrent-chemoi

Just a list of decent projects

# Generative Molecules

* https://github.com/chennnnnyize/Generative-Molecules
* https://github.com/ETHmodlab/virtual_libraries.git
* https://github.com/khinsen/MMTK

# QM calculations 

* ORCA  
* NWChem
* Quantumespresso 
* Psi4 (myfav)